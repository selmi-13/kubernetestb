var scoreboardAPI = require("../external-api/nba-scoreboards");
const moment = require("moment-timezone");
const os = require("os")

const getTodayGames = async () => {
  let string = "pod "+os.hostname();
  string += "\n\r"+"Les matchs du jour sont : ";
  /**
   * TODO : Generer un beau texte tout beau en
   * fonction des données reçus dans le response
   * au-dessus
   */
  const response = await scoreboardAPI.getTodayGames();

  response.games.map( game => {
    string+=( "\n\r"+ game.vTeam.triCode+ " vs " + game.hTeam.triCode);
    console.log(game.period.current);
    if(game.period.current > 0){
      string+=(" : "+game.vTeam.score+ " - " + game.hTeam.score + (game.period.current == 4 && game.isGameActivated ==false ? " FINAL" : ""));
    }else{
      string +=(" : start at "+ game.startTimeEastern+ " (heure Suisse : " +(moment.tz(game.startTimeUTC,'America/NewYork').clone().tz('Europe/Paris').format('HH:mm'))+")");
    }
  }); 
  for(i=0; i<1000;i++){
    console.log("stress/")
  }
  return string;
};

module.exports = {
  getTodayGames
};