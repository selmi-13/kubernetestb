const axios = require("axios");
const moment = require("moment-timezone");

const getTodayGames = async () => {
  const dateFormattedForAPI = moment()
    .tz("America/New_York")
    .format("YYYYMMDD");
  const response = await axios.get(
    `http://data.nba.net/10s/prod/v1/20190512/scoreboard.json`
  );
  return response.data;
};

module.exports = {
  getTodayGames
};