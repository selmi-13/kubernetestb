const mongoose = require("mongoose");

const Student = mongoose.model( "Student", {
    firstName: String,
    name: String,
    year: Number,
    profile: String
});

module.exports = Student